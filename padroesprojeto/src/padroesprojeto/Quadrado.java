package padroesprojeto;

public class Quadrado implements IFiguraBiDimensional{

	private int lado;
	
	public Quadrado() {
		this.lado = 5;
	}
	
	@Override
	public double perimetro() {
		return lado * 4;
	}

	@Override
	public double area() {
		return lado * lado;
	}
	
	@Override
	public String toString() {
		return "(" + this.lado + ", " + this.lado + ", " + this.lado + ", " + this.lado + ")";
	}

}
