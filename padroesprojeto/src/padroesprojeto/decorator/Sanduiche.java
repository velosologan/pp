package padroesprojeto.decorator;

//ConcreteComponent
public class Sanduiche implements ISanduiche {

	@Override
	public double valor() {
		return 1.2;
	}

	@Override
	public String descricao() {
		return "Pão";
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}
