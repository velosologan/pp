package padroesprojeto.decorator;

//Component
public interface ISanduiche {

	public double valor();
	
	public String descricao();
}
