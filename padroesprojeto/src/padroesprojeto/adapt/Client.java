package padroesprojeto.adapt;

public class Client {

		public static void main(String[] args) {
			
			Target t = new Adapter();

			t.exibirMensagem("ERR:Erro inesperado!");
			
			t.exibirMensagem("WARN:Cuidado!");
			
			t.exibirMensagem("ERR:Bugs 001!");
			
			t.exibirMensagem("Seja bem vindo!");
		}
}
