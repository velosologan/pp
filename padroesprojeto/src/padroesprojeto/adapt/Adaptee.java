package padroesprojeto.adapt;

public class Adaptee implements Target {

	@Override
	public void exibirMensagem(String msg) {
		System.out.println("######" + msg + "######");
	}
}
